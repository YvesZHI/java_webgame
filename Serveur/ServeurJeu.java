package Serveur;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import java.net.Socket;

import Data.*;


public class ServeurJeu extends Serveur
{
	private Tictactoes ts;
	private Thread td;

	public ServeurJeu(int port) throws IOException {
		super(port);
		this.ts = new Tictactoes();
		td = new Thread(new Runnable(){
			@Override
			public void run()
			{
				playTictactoe();
			}
		});
		td.start();
	}

	@Override
	protected void handleSocket(Socket socket)
	{
		try {
			BufferedReader inFromClient = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			ObjectOutputStream outToClient = new ObjectOutputStream(socket.getOutputStream());
			String clientMsg = inFromClient.readLine();
			System.out.println("Received from client: " + clientMsg);
			Object obj;
			try {
				System.out.println("building object...");
				obj = Class.forName("Data." + clientMsg).newInstance();
				System.out.println("object built");
				if(obj instanceof Pendu || obj instanceof Taquin)
				{
					System.out.println("Send to client: " + obj);
					outToClient.writeObject(obj);
					socket.close();
				}
				else if(obj instanceof Tictactoe)
				{
					synchronized(ts)
					{
						ts.add(socket);
					}
				}
				else
				{
					throw new ClassNotFoundException("no class named " + clientMsg);
				}
			} catch (InstantiationException e) {
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void playTictactoe()
	{
		synchronized(ts)
		{
			ts.play();
		}
	}
	
	public static void main(String[] args)
	{
		ServeurJeu sj;
		try {
			sj = new ServeurJeu(6789);
			sj.execute();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
