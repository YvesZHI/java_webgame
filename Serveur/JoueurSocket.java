package Serveur;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import Data.State;
import Data.TictactoeData;


public class JoueurSocket 
{
	private TictactoeData td;
	private Socket socket;
	
	public JoueurSocket(TictactoeData td, Socket socket)
	{
		this.td = td;
		this.socket = socket;
	}
	
	public TictactoeData getTictactoeData()
	{
		return this.td;
	}
	
	public void setTictactowData(TictactoeData td)
	{
		this.td = td;
	}
	
	public boolean isDone()
	{
		return td.getState() != State.waiting && td.getState() != State.playing;
	}
	
	public void send() throws IOException
	{
		ObjectOutputStream outToServer = new ObjectOutputStream(socket.getOutputStream());
		outToServer.writeObject(td);
	}

	public boolean receive() throws IOException, ClassNotFoundException
	{
		socket.setSoTimeout(50);
		InputStream in = socket.getInputStream();
		if(in == null)
		{
			return false;
		}
		ObjectInputStream ois = new ObjectInputStream(in);
		td = (TictactoeData)ois.readObject();
		return true;
	}
	
	public void close() throws IOException
	{
		socket.close();
	}
}
