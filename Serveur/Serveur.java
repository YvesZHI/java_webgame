package Serveur;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;


public abstract class Serveur
{
	private int port;
	private ServerSocket socket;

	public Serveur(int port) throws IOException
	{
		this.port = port;
		this.socket = new ServerSocket(port);
	}

	public int getPort()
	{
		return this.port;
	}

	public void execute() throws IOException
	{
		while(true)
		{
			System.out.println("accepting...");
			Socket connectionSocket = this.socket.accept();
			//ClientThread ct = new ClientThread(connectionSocket);
			//Thread td = new Thread(ct);
			//System.out.println("ClientThread starting...");
			//td.start();
			handleSocket(connectionSocket);
		}
	}
	
	protected abstract void handleSocket(Socket socket);
}
