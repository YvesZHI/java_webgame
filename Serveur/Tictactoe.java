package Serveur;

import java.io.IOException;

import Data.TictactoeData;
import Data.State;


public class Tictactoe
{
	public enum T_State{connecting, playing, ending};

	private Character[] chessBoard;
	private JoueurSocket j1, j2;

	public Tictactoe()
	{
		this.chessBoard = new Character[]{' ', ' ', ' ', ' ', ' ', ' ' , ' ', ' ', ' '};
	}

	public T_State getState()
	{
		if(j1 == null || j2 == null)
		{
			return T_State.connecting;
		}
		if(!j1.isDone() && !j2.isDone())
		{
			return T_State.playing;
		}
		return T_State.ending;
	}

	public JoueurSocket getJ1()
	{
		return j1;
	}

	public JoueurSocket getJ2()
	{
		return j2;
	}

	public void setJ1(JoueurSocket j)
	{
		j1 = j;
	}

	public void setJ2(JoueurSocket j)
	{
		j2 = j;
	}

	public void play()
	{
		try {
			if(getState() == T_State.connecting)
			{
				if(j1 != null)
				{
					j1.getTictactoeData().setState(State.waiting);
					j1.send();
				}
				if(j2 != null)
				{
					j1.getTictactoeData().setState(State.playing);
					j2.getTictactoeData().setState(State.playing);
					j1.send();
					j2.send();
				}
			}
			else if (getState() == T_State.playing)
			{
				if(j1.receive())
				{
					this.chessBoard[j1.getTictactoeData().getPos()] = j1.getTictactoeData().getCh();
					if(hasWon(j1.getTictactoeData()))
					{
						j1.getTictactoeData().setState(State.winning);
						j1.send();
						j2.getTictactoeData().setState(State.losing);
						j2.send();
						return;
					}
					j2.setTictactowData(j1.getTictactoeData());
					j2.send();
				}

				if(j2.receive())
				{
					// if received
					this.chessBoard[j2.getTictactoeData().getPos()] = j2.getTictactoeData().getCh();
					if(hasWon(j2.getTictactoeData()))
					{
						j2.getTictactoeData().setState(State.winning);
						j2.send();
						j1.getTictactoeData().setState(State.losing);
						j1.send();
						return;
					}
					j1.setTictactowData(j2.getTictactoeData());
					j1.send();
				}

				if(isDraw())
				{
					j1.getTictactoeData().setState(State.draw);
					j2.getTictactoeData().setState(State.draw);
					j1.send();
					j2.send();
				}
			}
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void close()
	{
		try {
			j1.close();
			j2.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	private boolean isDraw()
	{
		return false;
	}

	private boolean hasWon(TictactoeData td)
	{
		return false;
	}
}
