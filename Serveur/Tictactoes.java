package Serveur;

import java.net.Socket;

import Data.TictactoeData;

public class Tictactoes 
{
	private Tictactoe[] t;
	private int counter;
	
	public Tictactoes()
	{
		this.t = new Tictactoe[10];
		this.counter = 0;
	}
	
	public void add(Socket socket)
	{
		if(counter == 10)
		{
			// handle error msg
			return;
		}
		if(t[counter] == null)
		{
			t[counter] = new Tictactoe();
			t[counter].setJ1(new JoueurSocket(new TictactoeData('X'), socket));
		}
		else if(t[counter].getJ2() == null)
		{
			t[counter++].setJ2(new JoueurSocket(new TictactoeData('O'), socket));
		}
	}
	
	public void play()
	{
		for(int i = 0; i < counter; i++)
		{
			if(t[i].getState() == Tictactoe.T_State.ending)
			{
				t[i].close();
				t[i] = null;
			}
			t[i].play();
		}
	}
}
