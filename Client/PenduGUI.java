package Client;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import Data.Pendu;


public class PenduGUI extends JeuGUI
{
	class PenduPanel extends JPanel implements KeyListener
	{
		private static final long serialVersionUID = 1L;
		private String but;
		private String butToLower;
		private ArrayList<JLabel> labels;
		private int time;
		private int counter;
		private JLabel timeLabel;

		public PenduPanel(Object obj)
		{
			super();
			but = ((Pendu)obj).getBut();
			butToLower = but.toLowerCase();
			counter = 0;
			time = but.length();
			this.addKeyListener(this);
			this.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			labels = new ArrayList<JLabel>();
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 0.5;
			c.gridy = 0;
			for(int i = 0; i < but.length(); i++)
			{
				JLabel l = new JLabel(" ", SwingConstants.CENTER);
				l.setBorder(BorderFactory.createLineBorder(Color.BLUE));
				c.gridx = i;
				this.add(l, c);
				labels.add(l);
			}
			timeLabel = new JLabel(String.valueOf(time), SwingConstants.CENTER);
			c.gridx = 0;
			c.gridy = 4;
			this.add(timeLabel, c);
			c.gridx = but.length();
			c.gridy = 4;
			this.add(back, c);
			back.setVisible(false);
			this.revalidate();
			this.repaint();
			this.setVisible(true);
			this.setFocusable(true);
		}

		@Override
		public void addNotify() {
			super.addNotify();
			requestFocus();
		}

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e)
		{
			char c = e.getKeyChar();
			boolean found = false;
			if(Character.isLetter(c))
			{
				char cToLower = Character.toLowerCase(c);
				int index = butToLower.indexOf(cToLower);
				while (index >= 0)
				{
					found = true;
					if(labels.get(index).getText() == " ")
					{
						labels.get(index).setText(String.valueOf(but.charAt(index)));
						index = butToLower.indexOf(cToLower, index + 1);
						counter++;
					}
					else
					{
						break;
					}
				}
				if(counter == but.length())
				{
					for(int i = 0; i < labels.size(); i++)
					{
						labels.get(i).setBorder(BorderFactory.createEmptyBorder());
					}
					this.setFocusable(false);
					back.setVisible(true);
				}
				if(!found)
				{
					if(time-- == 0)
					{
						this.setFocusable(false);
						back.setVisible(true);
						return;
					}
					timeLabel.setText(String.valueOf(time));
				}
			}
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}
	}

	public PenduGUI(String str, Jeu jeu)
	{
		super(str, jeu.getPort());
		setButtonListener(jeu);
	}

	@Override
	protected JPanel designPanel()
	{
		try {
			this.client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new PenduPanel(obj);
	}
}