package Client;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

import Data.Taquin;

public class TaquinGUI extends JeuGUI
{
	class TaquinPanel extends JPanel implements KeyListener
	{
		private static final long serialVersionUID = 1L;
		private int[] elements;
		private ArrayList<JLabel> labels;
		private int currentPos;

		public TaquinPanel(Object obj)
		{
			super();
			elements = ((Taquin)obj).getInit();
			this.addKeyListener(this);
			this.setLayout(new GridBagLayout());
			GridBagConstraints c = new GridBagConstraints();
			labels = new ArrayList<JLabel>();
			int len = (int)Math.sqrt(elements.length);
			c.fill = GridBagConstraints.HORIZONTAL;
			c.weightx = 0.5;
			c.gridwidth = 1;
			c.gridwidth = 1;
			for(int i = 0; i < elements.length; i++)
			{
				JLabel l = new JLabel((elements[i] == 0 ? " " : String.valueOf(elements[i])), SwingConstants.CENTER);
				if(l.getText() == " ")
				{
					currentPos = i;
				}
				l.setBorder(BorderFactory.createLineBorder(Color.BLUE));
				c.gridx = i % len;
				c.gridy = i / len;
				this.add(l, c);
				labels.add(l);
			}
			c.weightx = 0.0;
			c.gridwidth = 2;
			c.gridy = 7;
			back.setText("give up");
			this.add(back, c);
			back.setVisible(true);
			this.revalidate();
			this.repaint();
			this.setVisible(true);
			this.setFocusable(true);
		}

		@Override
		public void addNotify() {
			super.addNotify();
			requestFocus();
		}

		@Override
		public void keyTyped(KeyEvent e) {
		}

		@Override
		public void keyPressed(KeyEvent e)
		{
			int keyCode = e.getKeyCode();
			int len = (int)Math.sqrt(elements.length);
		    switch( keyCode ) { 
		        case KeyEvent.VK_DOWN:
		        	if(currentPos >= len)
		        	{
		        		elements[currentPos] = elements[currentPos - len];
		        		elements[currentPos - len] = 0;
		        		labels.get(currentPos).setText(labels.get(currentPos - len).getText());
		        		labels.get(currentPos - len).setText(" ");
		        		currentPos -= len;
		        	}
		            break;
		        case KeyEvent.VK_UP:
		        	if(currentPos < (len - 1) * len)
		        	{
		        		elements[currentPos] = elements[currentPos + len];
		        		elements[currentPos + len] = 0;
		        		labels.get(currentPos).setText(labels.get(currentPos + len).getText());
		        		labels.get(currentPos + len).setText(" ");
		        		currentPos += len;
		        	}
		            break;
		        case KeyEvent.VK_RIGHT:
		        	if(currentPos % len != 0)
		        	{
		        		elements[currentPos] = elements[currentPos - 1];
		        		elements[currentPos - 1] = 0;
		        		labels.get(currentPos).setText(labels.get(currentPos - 1).getText());
		        		labels.get(currentPos - 1).setText(" ");
		        		currentPos -= 1;
		        	}
		            break;
		        case KeyEvent.VK_LEFT :
		        	if((currentPos + 1) % len != 0)
		        	{
		        		elements[currentPos] = elements[currentPos + 1];
		        		elements[currentPos + 1] = 0;
		        		labels.get(currentPos).setText(labels.get(currentPos + 1).getText());
		        		labels.get(currentPos + 1).setText(" ");
		        		currentPos += 1;
		        	}
		            break;
		     }
		    if(currentPos == elements.length - 1)
		    {
		    	if(((Taquin)obj).gagne(elements))
		    	{
		    		back.setText("you win!");
		    		this.setFocusable(false);
		    	}
		    }
		}

		@Override
		public void keyReleased(KeyEvent e) {
		}
	}


	public TaquinGUI(String str, Jeu jeu)
	{
		super(str, jeu.getPort());
		setButtonListener(jeu);
	}

	@Override
	protected JPanel designPanel() 
	{
		try {
			this.client.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new TaquinPanel(obj);
	}
}
