package Client;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

import Data.*;


public class Jeu extends JFrame
{
	private static final long serialVersionUID = 1L;
	private JPanel panelButton;
	private JeuGUI pendu, taquin;
	private int port;

	public Jeu(int port)
	{
		this.port = port;
		this.setSize(400, 400);
		pendu = new PenduGUI("Pendu", this);
		taquin = new TaquinGUI("Taquin", this);
		setDefaultLookAndFeelDecorated(true);
		panelButton = new JPanel();
		panelButton.setLayout(new BorderLayout());
		panelButton.add(pendu.getButton(), BorderLayout.BEFORE_FIRST_LINE);
		panelButton.add(taquin.getButton(), BorderLayout.AFTER_LAST_LINE);
		this.getContentPane().add(panelButton);
		this.revalidate();
		this.repaint();
		setVisible(true);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
	}
	
	public int getPort()
	{
		return port;
	}
	
	public void initPanel()
	{
		this.getContentPane().removeAll();
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(panelButton);
		this.revalidate();
		this.repaint();
	}

	public void setPanel(JPanel p)
	{
		this.getContentPane().removeAll();
		this.getContentPane().setLayout(new BorderLayout());
		this.getContentPane().add(p);
		this.revalidate();
		this.repaint();
	}

	public static void main(String argv[])
	{
		Jeu jeu = new Jeu(6789);
	}
}
