package Client;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.net.UnknownHostException;


public class Client
{
	private int port;
	private Socket clientSocket;

	public Client(int port) throws UnknownHostException, IOException, ClassNotFoundException
	{
		this("localhost", port);
		this.port = port;
	}

	public Client(String ip, int port) throws UnknownHostException, IOException, ClassNotFoundException
	{
		this.port = port;
		this.clientSocket = new Socket("localhost", port);
	}

	public int getPort()
	{
		return this.port;
	}

	public void send(String msg) throws IOException
	{
		DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());
		outToServer.writeBytes(msg + '\n');
	}

	public Object receive() throws IOException, ClassNotFoundException
	{
		ObjectInputStream ois = new ObjectInputStream(clientSocket.getInputStream());
		return ois.readObject();
	}
	
	public void close() throws IOException
	{
		this.clientSocket.close();
	}
}
