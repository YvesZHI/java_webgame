package Client;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


public abstract class JeuGUI
{	
	protected JButton button;
	protected JPanel panel;
	protected Object obj;
	protected JButton back;
	protected Client client;

	public JeuGUI(String str, int port)
	{
		button = new JButton(str);
		back = new JButton("back");
	}

	protected void setButtonListener(Jeu jeu)
	{
		button.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				try {
					client = new Client(jeu.getPort());
					client.send(button.getText());
					System.out.println("sent: " + button.getText());
					System.out.println("receiving msg from server...");
					obj = client.receive();
					System.out.println("received: " + obj);
					jeu.setPanel(designPanel());
				} catch (ClassNotFoundException e1) {
					System.err.println("aaaaaa");
					e1.printStackTrace();
				} catch (IOException e1) {
					System.err.println("bbbbbb");
					e1.printStackTrace();
				}
			}
		});
		back.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				jeu.initPanel();
			}
		});
	}

	protected abstract JPanel designPanel();

	public JButton getButton()
	{
		return button;
	}
}
