package Data;

import java.io.Serializable;


public class TictactoeData implements Serializable
{
	private static final long serialVersionUID = 1L;
	
	public class Piece
	{
		private char ch;
		
		public Piece(char ch)
		{
			this.ch = ch;
		}
		
		public char getCh()
		{
			return this.ch;
		}
	}
	
	private State state;
	private Piece ch;
	private int pos;
	
	public TictactoeData(char ch)
	{
		this.ch = new Piece(ch);
		this.pos = -1;
		this.state = State.waiting;
	}

	public TictactoeData(char ch, int pos)
	{
		this.ch = new Piece(ch);
		this.pos = pos;
		this.state = State.playing;
	}
	
	public char getCh()
	{
		return this.ch.getCh();
	}
	
	public int getPos()
	{
		return this.pos;
	}

	public State getState()
	{
		return this.state;
	}
	
	public void setState(State state)
	{
		this.state = state;
	}
}
