package Data;

import java.io.Serializable;
import java.util.Random;


public class Pendu implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static String[] words = new String[]{"bonjour", "bonsoir", "hier", "france", "ordinateur"};
	private static Random rand = new Random();
	private String but;
	
	public Pendu()
	{
		this.but = words[rand.nextInt(words.length)];
	}
	
	public int getLenth()
	{
		return this.but.length();
	}
	
	public String getBut()
	{
		return this.but;
	}
	
	@Override
	public String toString()
	{
		return this.but;
	}
}
