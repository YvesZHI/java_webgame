package Data;

import java.io.Serializable;

public class Joueur implements Serializable
{
	private static final long serialVersionUID = 1L;
	private char ch;
	private int pos;
	
	public Joueur(char ch)
	{
		this.ch = ch;
	}
	
	public char getCh()
	{
		return ch;
	}
	
	public void setPos(int p)
	{
		pos = p;
	}
	
	public int getPos()
	{
		return pos;
	}
}
