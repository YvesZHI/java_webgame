package Data;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Random;


public class Taquin implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static Random rand = new Random();
	private int[] res;

	public Taquin()
	{
		this.res = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 0};
	}

	public boolean gagne(int[] tab)
	{
		return Arrays.equals(this.res, tab);
	}

	public int[] getInit()
	{
		int[] init = new int[this.res.length];
		System.arraycopy(this.res, 0, init, 0, this.res.length);
		for(int i = init.length - 1; i > 0; i--)
		{
			int index = rand.nextInt(i + 1);
			int a = init[index];
			init[index] = init[i];
			init[i] = a;
		}
		return init;
	}

	@Override
	public String toString()
	{
		return String.valueOf(this.getInit().length);
	}
}
